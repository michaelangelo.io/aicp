// Important note: this file is taken from the go-git project.
// Please, refer to the original project: https://pkg.go.dev/github.com/go-git/go-git/v5@v5.12.0/plumbing/format/gitignore
// The main change is ReadPatterns, it was changed so that we can leverage concurrency to speed up the process of reading the gitignore files.

package main

import (
	"bufio"
	"fmt"
	"os"
	"os/user"
	"path/filepath"
	"strings"
	"sync"

	"github.com/go-git/go-billy/v5"
	"github.com/go-git/go-billy/v5/osfs"
	"github.com/go-git/go-git/v5/plumbing/format/gitignore"
)

const (
	commentPrefix   = "#"
	coreSection     = "core"
	excludesfile    = "excludesfile"
	gitDir          = ".git"
	gitignoreFile   = ".gitignore"
	gitconfigFile   = ".gitconfig"
	systemFile      = "/etc/gitconfig"
	infoExcludeFile = gitDir + "/info/exclude"
)

// readIgnoreFile reads a specific git ignore file.
// should match the original function from the go-git project.
func readIgnoreFile(fs billy.Filesystem, path []string, ignoreFile string) (ps []gitignore.Pattern, err error) {
	ignoreFile, _ = ReplaceTildeWithHome(ignoreFile)

	f, err := fs.Open(fs.Join(append(path, ignoreFile)...))
	if err == nil {
		defer f.Close()

		scanner := bufio.NewScanner(f)
		for scanner.Scan() {
			s := scanner.Text()
			if !strings.HasPrefix(s, commentPrefix) && len(strings.TrimSpace(s)) > 0 {
				ps = append(ps, gitignore.ParsePattern(s, path))
			}
		}
	} else if !os.IsNotExist(err) {
		return nil, err
	}

	return
}

// ReadPatterns reads the .git/info/exclude and then the gitignore patterns
// recursively traversing through the directory structure. The result is in
// the ascending order of priority (last higher).
// ^ This is the changed function from the original project.
func ReadPatterns(fs billy.Filesystem, path []string) (ps []gitignore.Pattern, err error) {
	ps, _ = readIgnoreFile(fs, path, infoExcludeFile)

	subps, _ := readIgnoreFile(fs, path, gitignoreFile)
	ps = append(ps, subps...)

	var fis []os.FileInfo
	fis, err = fs.ReadDir(fs.Join(path...))
	if err != nil {
		return
	}

	var wg sync.WaitGroup
	var mu sync.Mutex
	for _, fi := range fis {
		if fi.IsDir() && fi.Name() != gitDir {
			wg.Add(1)
			go func(subPath []string) {
				defer wg.Done()
				subps, err := ReadPatterns(fs, subPath)
				if err != nil {
					return
				}

				if len(subps) > 0 {
					mu.Lock()
					ps = append(ps, subps...)
					mu.Unlock()
				}
			}(append(path, fi.Name()))
		}
	}
	wg.Wait()

	return
}

// should match the original function from the go-git project.
func ReplaceTildeWithHome(path string) (string, error) {
	if strings.HasPrefix(path, "~") {
		firstSlash := strings.Index(path, "/")
		if firstSlash == 1 {
			home, err := os.UserHomeDir()
			if err != nil {
				return path, err
			}
			return strings.Replace(path, "~", home, 1), nil
		} else if firstSlash > 1 {
			username := path[1:firstSlash]
			userAccount, err := user.Lookup(username)
			if err != nil {
				return path, err
			}
			return strings.Replace(path, path[:firstSlash], userAccount.HomeDir, 1), nil
		}
	}

	return path, nil
}

/*
This function is used to find the parent .gitconfig file in the directory tree.
*/
func findParentGitConfig(dir string) string {
	gitConfigPath := filepath.Join(dir, ".git", "config")
	if _, err := os.Stat(gitConfigPath); err == nil {
		return dir
	}

	parentDir := filepath.Dir(dir)
	if parentDir == dir {
		return ""
	}

	return findParentGitConfig(parentDir)
}

/*
This function is used to get the git matcher for the given path.

We find the parent .gitconfig file and use that path as the entry point
to find all the gitignore files in the directory tree.

The patterns are then used to create a gitignore.Matcher.
*/
func getGitMatcherForPath(path string) (*gitignore.Matcher, string, error) {
	// Find the .gitconfig file in the directory tree.
	gitConfigPath := findParentGitConfig(path)
	if gitConfigPath == "" {
		return nil, gitConfigPath, fmt.Errorf("could not find .gitconfig file in the directory tree")
	}

	fs := osfs.New(gitConfigPath)
	patterns, err := ReadPatterns(fs, nil)
	if err != nil {
		return nil, gitConfigPath, err
	}

	matcher := gitignore.NewMatcher(patterns)
	return &matcher, gitConfigPath, nil
}
