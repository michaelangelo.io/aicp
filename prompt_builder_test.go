package main

import (
	"os"
	"path/filepath"
	"strings"
	"testing"
)

func TestBuildPrompt(t *testing.T) {
	// Create a temporary directory for test files
	tempDir, err := os.MkdirTemp("", "test_prompt_builder")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v", err)
	}
	defer os.RemoveAll(tempDir)

	// Create test files
	testFiles := []struct {
		name    string
		content string
	}{
		{"file1.txt", "This is file 1"},
		{"file2.go", "package main\nfunc main() {}"},
	}

	var finalFiles []FinalFile
	for _, tf := range testFiles {
		path := filepath.Join(tempDir, tf.name)
		if err := os.WriteFile(path, []byte(tf.content), 0644); err != nil {
			t.Fatalf("Failed to write test file: %v", err)
		}
		finalFiles = append(finalFiles, FinalFile{
			RelFilepath: tf.name,
			Size:        int64(len(tf.content)),
			Ext:         filepath.Ext(tf.name),
			ModTime:     0, // Use 0 for consistency in tests
		})
	}

	// Test buildPrompt function
	progress := make(chan int)
	go func() {
		for range progress {
			// Consume progress updates
		}
	}()

	prompt, err := buildPrompt(tempDir, finalFiles, progress)
	if err != nil {
		t.Fatalf("buildPrompt failed: %v", err)
	}

	// Check if the prompt matches the expected format exactly
	expectedPrompt := "------ file1.txt ------\n" +
		"``````\n" +
		"This is file 1\n\n" +
		"``````\n\n" +
		"------ file2.go ------\n" +
		"``````\n" +
		"package main\n" +
		"func main() {}\n\n" +
		"``````\n"

	// Normalize line endings
	prompt = strings.ReplaceAll(prompt, "\r\n", "\n")
	expectedPrompt = strings.ReplaceAll(expectedPrompt, "\r\n", "\n")

	if prompt != expectedPrompt {
		t.Errorf("Prompt does not match expected format.\nExpected:\n%s\nGot:\n%s", expectedPrompt, prompt)
	}
}
