package main

import (
	"testing"
	"time"

	"github.com/charmbracelet/bubbles/list"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/stretchr/testify/assert"
)

func TestNewFinalFileListItem(t *testing.T) {
	file := FinalFile{
		RelFilepath: "test/file.txt",
		Size:        1024,
		Ext:         ".txt",
		ModTime:     time.Now().Unix(),
	}

	item := newFinalFileListItem(file)

	assert.Equal(t, file, item.file)
	assert.True(t, item.selected)
	assert.NotEmpty(t, item.modTimeStr)
	assert.Equal(t, "1.0 kB", item.sizeStr)
}

func TestFinalFileListItemFilterValue(t *testing.T) {
	item := finalFileListItem{
		file: FinalFile{RelFilepath: "test/file.txt"},
	}

	assert.Equal(t, "test/file.txt", item.FilterValue())
}

func TestFinalFileListItemTitle(t *testing.T) {
	item := finalFileListItem{
		file: FinalFile{RelFilepath: "test/file.txt"},
	}

	assert.Equal(t, "test/file.txt", item.Title())
}

func TestFinalFileListItemDescription(t *testing.T) {
	item := finalFileListItem{
		file:       FinalFile{Ext: ".txt"},
		selected:   true,
		modTimeStr: "2023-06-24",
		sizeStr:    "1.0 kB",
	}

	desc := item.Description()
	assert.Contains(t, desc, "[selected]")
	assert.Contains(t, desc, "Size: 1.0 kB")
	assert.Contains(t, desc, "Extension: .txt")
	assert.Contains(t, desc, "Modified: 2023-06-24")
}

func TestFinalFilesToItems(t *testing.T) {
	files := []FinalFile{
		{RelFilepath: "file1.txt"},
		{RelFilepath: "file2.txt"},
	}

	items := finalFilesToItems(&files)

	assert.Len(t, items, 2)
	assert.Equal(t, "file1.txt", items[0].(finalFileListItem).file.RelFilepath)
	assert.Equal(t, "file2.txt", items[1].(finalFileListItem).file.RelFilepath)
}

func TestNewFileListModel(t *testing.T) {
	model := newFileListModel("/test/path")

	assert.Equal(t, "/test/path", model.searchPath)
	assert.Nil(t, model.err)
	assert.False(t, model.viewPrompt)
	assert.Equal(t, "AI Copy/Paste - Select Files To Copy", model.list.Title)
}

func TestFileListModelGetSelectedFiles(t *testing.T) {
	model := newFileListModel("/test/path")
	model.list.SetItems([]list.Item{
		finalFileListItem{file: FinalFile{RelFilepath: "file1.txt"}, selected: true},
		finalFileListItem{file: FinalFile{RelFilepath: "file2.txt"}, selected: false},
		finalFileListItem{file: FinalFile{RelFilepath: "file3.txt"}, selected: true},
	})

	selectedFiles := model.GetSelectedFiles()

	assert.Len(t, selectedFiles, 2)
	assert.Equal(t, "file1.txt", selectedFiles[0].RelFilepath)
	assert.Equal(t, "file3.txt", selectedFiles[1].RelFilepath)
}

func TestFileListModelUpdate(t *testing.T) {
	model := newFileListModel("/test/path")
	model.list.SetItems([]list.Item{
		finalFileListItem{file: FinalFile{RelFilepath: "file1.txt"}, selected: true},
	})

	initialModelState := initialModel("/test/path", false, false)

	// Test 's' key press (select/deselect)
	newModel, cmd := model.Update(tea.KeyMsg{Type: tea.KeyRunes, Runes: []rune{'s'}}, initialModelState)
	assert.Nil(t, cmd)
	assert.False(t, newModel.list.SelectedItem().(finalFileListItem).selected)

	// Test 'c' key press (copy)
	newModel, cmd = model.Update(tea.KeyMsg{Type: tea.KeyRunes, Runes: []rune{'c'}}, initialModelState)
	assert.Nil(t, cmd)

	// Test 'p' key press (view prompt)
	newModel, cmd = model.Update(tea.KeyMsg{Type: tea.KeyRunes, Runes: []rune{'p'}}, initialModelState)
	assert.Nil(t, cmd)
}

func TestFileListModelView(t *testing.T) {
	model := newFileListModel("/test/path")

	view := model.View()
	assert.NotEmpty(t, view)

	model.err = assert.AnError
	errorView := model.View()
	assert.Contains(t, errorView, "Error:")
}
