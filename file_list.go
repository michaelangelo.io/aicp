package main

import (
	"fmt"
	"time"

	"github.com/atotto/clipboard"
	"github.com/charmbracelet/bubbles/key"
	list "github.com/charmbracelet/bubbles/list"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"github.com/dustin/go-humanize"
)

var (
	appStyle  = lipgloss.NewStyle().Padding(1, 2)
	listStyle = lipgloss.NewStyle().Border(lipgloss.RoundedBorder())
)

type finalFileListItem struct {
	file       FinalFile
	selected   bool
	modTimeStr string
	sizeStr    string
}

func newFinalFileListItem(file FinalFile) finalFileListItem {
	var modTimeStr string
	modTime := time.Unix(file.ModTime, 0)
	if modTime.Format("2006-01-02") == time.Now().Format("2006-01-02") {
		modTimeStr = modTime.Format("15:04:05")
	} else {
		modTimeStr = modTime.Format("2006-01-02")
	}
	size := humanize.Bytes(uint64(file.Size))
	return finalFileListItem{
		file:       file,
		selected:   true,
		modTimeStr: modTimeStr,
		sizeStr:    size,
	}
}

func (i finalFileListItem) FilterValue() string {
	return i.file.RelFilepath
}

func (i finalFileListItem) Title() string {
	return i.file.RelFilepath
}

func (i finalFileListItem) Description() string {
	var selectStatus string
	if i.selected {
		selectStatus = lipgloss.NewStyle().Foreground(lipgloss.Color("69")).Render("[selected]")
	} else {
		selectStatus = lipgloss.NewStyle().Foreground(lipgloss.Color("241")).Render("[press 's' to select]")
	}

	return fmt.Sprintf("%s Size: %s, Extension: %s, Modified: %s",
		selectStatus, i.sizeStr, i.file.Ext, i.modTimeStr)
}

func finalFilesToItems(files *[]FinalFile) []list.Item {
	finalFileListItems := make([]list.Item, 0, len(*files))
	for _, file := range *files {
		finalFileListItems = append(finalFileListItems, newFinalFileListItem(file))
	}
	return finalFileListItems
}

type fileListModel struct {
	list       list.Model
	err        error
	searchPath string
	viewPrompt bool
}

func newFileListModel(searchPath string) fileListModel {
	items := []list.Item{}

	delegate := list.NewDefaultDelegate()
	delegate.ShowDescription = true

	listModel := list.New(items, delegate, 0, 0)
	listModel.Title = "AI Copy/Paste - Select Files To Copy"
	listModel.Styles.Title = lipgloss.NewStyle().Background(lipgloss.Color("62"))
	listModel.StatusMessageLifetime = 10 * time.Second
	listModel.SetStatusBarItemName("file found", "files found")
	listModel.AdditionalShortHelpKeys = func() []key.Binding {
		return []key.Binding{
			key.NewBinding(key.WithKeys("s"), key.WithHelp("s", "select item")),
			key.NewBinding(key.WithKeys("c"), key.WithHelp("c", "copy files")),
			key.NewBinding(key.WithKeys("a"), key.WithHelp("a", "select all")),
			key.NewBinding(key.WithKeys("p"), key.WithHelp("p", "view prompt")),
		}
	}

	return fileListModel{
		list:       listModel,
		err:        nil,
		searchPath: searchPath,
	}
}

func (m fileListModel) Init() tea.Cmd {
	return nil
}

func (m fileListModel) GetSelectedFiles() []FinalFile {
	var selectedFiles []FinalFile
	for _, item := range m.list.VisibleItems() {
		if i, ok := item.(finalFileListItem); ok && i.selected {
			selectedFiles = append(selectedFiles, i.file)
		}
	}
	return selectedFiles
}

func (m fileListModel) IsFiltering() bool {
	return m.list.FilterState() == list.Filtering
}

func (m fileListModel) Update(msg tea.Msg, model model) (fileListModel, tea.Cmd) {
	if model.showPrompt {
		return m, nil
	}
	var cmd tea.Cmd
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "s":
			if m.IsFiltering() {
				break
			}
			index := m.list.Index()
			item := m.list.SelectedItem()
			if i, ok := item.(finalFileListItem); ok {
				i.selected = !i.selected
				m.list.RemoveItem(index)
				m.list.InsertItem(index, i)
			}
			selectedFiles := m.GetSelectedFiles()
			m.list.NewStatusMessage(fmt.Sprintf("Selected files: %d", len(selectedFiles)))
			m.list, cmd = m.list.Update(msg)
			return m, tea.Batch(cmd)
		case "a":
			if m.list.FilterState() == list.Filtering {
				break
			}
			for index, item := range m.list.Items() {
				if i, ok := item.(finalFileListItem); ok {
					i.selected = !i.selected
					m.list.RemoveItem(index)
					m.list.InsertItem(index, i)
				}
			}
			selectedFiles := m.GetSelectedFiles()
			m.list.NewStatusMessage(fmt.Sprintf("Selected files: %d", len(selectedFiles)))
			m.list, cmd = m.list.Update(msg)
			return m, tea.Batch(cmd)
		case "c":
			if m.IsFiltering() {
				break
			}
			selectedFiles := m.GetSelectedFiles()
			if len(selectedFiles) > 0 {
				progressChan := make(chan int)
				promptString, buildPromptErr := buildPrompt(m.searchPath, selectedFiles, progressChan)
				clipboardErr := clipboard.WriteAll(promptString)
				for i := range progressChan {
					progressMsg := fmt.Sprintf("Copying %d files...", i)
					m.list.NewStatusMessage(progressMsg)
				}
				if buildPromptErr != nil {
					errMsg := fmt.Sprintf("Error building prompt: %v\n", buildPromptErr)
					m.list.NewStatusMessage(errMsg)
				} else if clipboardErr != nil {
					errMsg := fmt.Sprintf("Error copying to clipboard: %v\n", clipboardErr)
					m.list.NewStatusMessage(errMsg)
				} else {
					progressMsg := fmt.Sprintf("Copied %d files ✅", len(selectedFiles))
					m.list.NewStatusMessage(progressMsg)
				}
				m.list, cmd = m.list.Update(msg)
				return m, tea.Batch(cmd)
			}
		case "p":
			if m.IsFiltering() {
				break
			}
			selectedFiles := m.GetSelectedFiles()
			if len(selectedFiles) > 0 {
				progressChan := make(chan int)
				promptString, err := buildPrompt(m.searchPath, selectedFiles, progressChan)
				if err != nil {
					errMsg := fmt.Sprintf("Error building prompt: %v\n", err)
					m.list.NewStatusMessage(errMsg)
				} else {
					m.viewPrompt = !m.viewPrompt
				}
				m.list, cmd = m.list.Update(msg)
				return m, tea.Batch(cmd, sendViewPromptMsg(promptString, true))
			}
		}
	}
	m.list, cmd = m.list.Update(msg)
	return m, cmd
}

func (m fileListModel) View() string {
	if m.err != nil {
		return fmt.Sprintf("Error: %v", m.err)
	}
	return appStyle.Render(listStyle.Render(m.list.View()))
}
