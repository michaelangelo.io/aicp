package main

import (
	"os"
	"path/filepath"
	"testing"
)

func TestRepoTraversal(t *testing.T) {
	// Create a temporary directory for test files
	tempDir, err := os.MkdirTemp("", "test_repo_traversal")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v", err)
	}
	defer os.RemoveAll(tempDir)

	// Create test files
	testFiles := []struct {
		name     string
		content  string
		expected bool
	}{
		{"file1.txt", "Regular file", true},
		{"test_file.go", "Test file", true}, // Now expected to be true as we're including test files
		{"image.jpg", "Image file", false},  // Should be excluded as a media file
	}

	for _, tf := range testFiles {
		path := filepath.Join(tempDir, tf.name)
		if err := os.WriteFile(path, []byte(tf.content), 0644); err != nil {
			t.Fatalf("Failed to write test file: %v", err)
		}
	}

	// Create RepoTraversal instance without Git
	rt, err := NewRepoTraversal(tempDir, false, true)
	if err != nil {
		t.Fatalf("Failed to create RepoTraversal: %v", err)
	}

	// Test WalkRepo
	finalFiles, err := rt.WalkRepo()
	if err != nil {
		t.Fatalf("WalkRepo failed: %v", err)
	}

	// Check if the expected files are present
	for _, tf := range testFiles {
		found := false
		for _, ff := range *finalFiles {
			if ff.RelFilepath == tf.name {
				found = true
				if !tf.expected {
					t.Errorf("Unexpected file in results: %s", tf.name)
				}
				break
			}
		}
		if tf.expected && !found {
			t.Errorf("Expected file not found in results: %s", tf.name)
		}
	}

	// Test ProcessFile
	testFile := filepath.Join(tempDir, "test_process.txt")
	if err := os.WriteFile(testFile, []byte("Test content"), 0644); err != nil {
		t.Fatalf("Failed to write test file: %v", err)
	}

	finalFile, err := rt.ProcessFile(testFile, []string{"test_process.txt"})
	if err != nil {
		t.Fatalf("ProcessFile failed: %v", err)
	}

	if finalFile == nil {
		t.Fatal("ProcessFile returned nil FinalFile")
	}

	if finalFile.RelFilepath != "test_process.txt" {
		t.Errorf("Expected RelFilepath to be 'test_process.txt', got '%s'", finalFile.RelFilepath)
	}
}

func TestMatchesFilePattern(t *testing.T) {
	rt := &RepoTraversal{}

	testCases := []struct {
		filePath string
		pattern  FilePattern
		expected bool
	}{
		{"test_file.go", testFilePattern, true},
		{"regular_file.go", testFilePattern, false},
		{"image.jpg", mediaFilePattern, true},
		{"document.txt", mediaFilePattern, false},
	}

	for _, tc := range testCases {
		result := rt.matchesFilePattern(tc.filePath, tc.pattern)
		if result != tc.expected {
			t.Errorf("matchesFilePattern(%s, %v) = %v, expected %v", tc.filePath, tc.pattern, result, tc.expected)
		}
	}
}
