package main

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"sync"
)

func buildPrompt(searchPath string, finalFiles []FinalFile, progress chan<- int) (string, error) {
	var wg sync.WaitGroup
	fileContents := make([]string, len(finalFiles))
	errs := make(chan error, len(finalFiles))

	for i, file := range finalFiles {
		wg.Add(1)
		go func(i int, file FinalFile) {
			defer wg.Done()

			filePath := filepath.Join(searchPath, file.RelFilepath)
			f, err := os.Open(filePath)
			if err != nil {
				errs <- fmt.Errorf("error opening file %s: %v", filePath, err)
				return
			}
			defer f.Close()

			var contentBuilder strings.Builder
			scanner := bufio.NewScanner(f)
			for scanner.Scan() {
				contentBuilder.WriteString(scanner.Text() + "\n")
			}

			if err := scanner.Err(); err != nil {
				errs <- fmt.Errorf("error reading file %s: %v", filePath, err)
				return
			}

			fileContent := fmt.Sprintf("------ %s ------\n``````\n%s\n``````\n", file.RelFilepath, contentBuilder.String())
			fileContents[i] = fileContent
		}(i, file)
	}

	go func() {
		wg.Wait()
		close(progress)
		close(errs)
	}()

	for err := range errs {
		if err != nil {
			return "", err
		}
	}

	prompt := strings.Join(fileContents, "\n")

	return prompt, nil
}
