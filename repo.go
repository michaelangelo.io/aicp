package main

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"sync"

	"github.com/go-git/go-git/v5/plumbing/format/gitignore"
	"github.com/karrick/godirwalk"
)

type FilePattern string

const (
	testFilePattern  FilePattern = `(?i)(?:^|[/\\])[._]?(?:test|spec)(?:[._].+)?\.(?:rb|js|py|ts|go|java|scala|php|swift|c|cpp|cs|clj|ex|erl|rs|hs)$|(?:^|[/\\])[._]?\w+[._](?:test|spec)\.(?:rb|js|py|ts|go|java|scala|php|swift|c|cpp|cs|clj|ex|erl|rs|hs)$`
	mediaFilePattern FilePattern = `(?i)\.(mp3|mp4|avi|mov|wmv|flv|mkv|wav|ogg|jpg|jpeg|png|gif|bmp|tiff|webm|webp|m4a|aac|flac|mpeg|mpg|3gp|m4v|avi|mkv|wasm)$`
)

type FinalFile struct {
	RelFilepath string
	Size        int64
	Ext         string
	ModTime     int64
}

type RepoTraversal struct {
	IncludeTestFiles *bool
	GitMatcher       *gitignore.Matcher
	GitConfigPath    *string
	searchPath       *string
	mu               sync.Mutex
}

func NewRepoTraversal(searchPath string, useGit bool, includeTestFiles bool) (*RepoTraversal, error) {
	if !useGit {
		return &RepoTraversal{
			IncludeTestFiles: &includeTestFiles,
			GitMatcher:       nil,
			GitConfigPath:    nil,
			searchPath:       &searchPath,
		}, nil
	}

	matcher, gitConfigPath, err := getGitMatcherForPath(searchPath)
	if err != nil {
		return nil, err
	}

	return &RepoTraversal{
		IncludeTestFiles: &includeTestFiles,
		GitMatcher:       matcher,
		GitConfigPath:    &gitConfigPath,
		searchPath:       &searchPath,
	}, nil
}

var regexCompileCache = map[FilePattern]*regexp.Regexp{
	testFilePattern:  regexp.MustCompile(string(testFilePattern)),
	mediaFilePattern: regexp.MustCompile(string(mediaFilePattern)),
}

func (dt *RepoTraversal) matchesFilePattern(filePath string, filePattern FilePattern) bool {
	return regexCompileCache[filePattern].MatchString(filePath)
}

func (dt *RepoTraversal) ProcessFile(path string, pathParts []string) (*FinalFile, error) {
	relPath, err := filepath.Rel(*dt.searchPath, path)
	if err != nil {
		return nil, err
	}

	ext := filepath.Ext(relPath)

	if !*dt.IncludeTestFiles && dt.matchesFilePattern(relPath, testFilePattern) {
		return nil, nil
	}

	if dt.matchesFilePattern(relPath, mediaFilePattern) {
		return nil, nil
	}

	if dt.GitMatcher != nil && (*dt.GitMatcher).Match(pathParts, false) {
		return nil, nil
	}

	fileInfo, err := os.Stat(path)
	if err != nil {
		return nil, err
	}

	return &FinalFile{
		RelFilepath: relPath,
		Size:        fileInfo.Size(),
		Ext:         ext,
		ModTime:     fileInfo.ModTime().Unix(),
	}, nil
}

// This will walk the repository and send the final files to the finalFileChain channel
// Skips .git directories and files that match the gitignore rules
// (if a .gitignore file is present and useGit is true)

func (dt *RepoTraversal) WalkRepo() (*[]FinalFile, error) {
	var wg sync.WaitGroup

	sem := make(chan struct{}, 20)

	var finalFiles []FinalFile

	err := godirwalk.Walk(*dt.searchPath,
		&godirwalk.Options{
			Unsorted: true,
			Callback: func(path string, de *godirwalk.Dirent) error {
				var err error
				var relPath string
				if dt.GitConfigPath == nil {
					relPath, err = filepath.Rel(*dt.searchPath, path)
				} else {
					relPath, err = filepath.Rel(*dt.GitConfigPath, path)
				}

				if err != nil {
					return err
				}

				pathParts := strings.Split(relPath, string(os.PathSeparator))

				if de.IsDir() {
					if de.Name() == ".git" {
						return filepath.SkipDir
					}

					if dt.GitMatcher != nil && (*dt.GitMatcher).Match(pathParts, false) {
						return filepath.SkipDir
					}

					return nil
				}

				wg.Add(1)
				sem <- struct{}{}
				go func() {
					defer func() {
						<-sem
					}()
					defer wg.Done()
					finalFile, err := dt.ProcessFile(path, pathParts)
					if err != nil {
						fmt.Printf("Error processing file: %v\n", err)
						return
					}
					if finalFile == nil {
						return
					}
					finalFiles = append(finalFiles, *finalFile)

				}()

				return nil
			},
		})

	wg.Wait()

	if err != nil {
		fmt.Printf("Error walking repository: %v\n", err)
		return nil, err
	}

	return &finalFiles, nil
}
