package main

import (
	"fmt"
	"os"

	"github.com/charmbracelet/bubbles/spinner"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"github.com/spf13/cobra"
)

type model struct {
	fileList        fileListModel
	spinner         spinner.Model
	loading         bool
	err             error
	searchPath      string
	useGit          bool
	ignoreTestFiles bool
	promptViewer    promptViewer
	showPrompt      bool
}

type viewPromptMsg struct {
	prompt     string
	showPrompt bool
}

func sendViewPromptMsg(prompt string, showPrompt bool) tea.Cmd {
	return func() tea.Msg {
		return viewPromptMsg{prompt: prompt, showPrompt: showPrompt}
	}
}

func initialModel(searchPath string, useGit bool, ignoreTestFiles bool) model {
	s := spinner.New()
	s.Spinner = spinner.Dot
	s.Style = lipgloss.NewStyle().Foreground(lipgloss.Color("205"))
	return model{
		fileList:        newFileListModel(searchPath),
		spinner:         s,
		loading:         true,
		err:             nil,
		searchPath:      searchPath,
		useGit:          useGit,
		ignoreTestFiles: ignoreTestFiles,
		promptViewer:    newPromptViewer(""),
		showPrompt:      false,
	}
}

func (m model) Init() tea.Cmd {
	return tea.Batch(m.RunProgram, m.spinner.Tick)
}

type fileListMsg struct {
	finalFiles *[]FinalFile
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "q":
			return m, tea.Quit
		}
	case tea.WindowSizeMsg:
		m.fileList.list.SetSize(msg.Width-4, msg.Height-4)
	case spinner.TickMsg:
		if m.loading {
			var cmd tea.Cmd
			m.spinner, cmd = m.spinner.Update(msg)
			return m, cmd
		}
	case fileListMsg:
		if msg.finalFiles != nil {
			finalFileListItems := finalFilesToItems(msg.finalFiles)
			m.fileList.list.SetItems(finalFileListItems)
		}
		m.loading = false
	case viewPromptMsg:
		m.promptViewer.viewport.SetContent(msg.prompt)
		m.showPrompt = msg.showPrompt
	}
	var fileListCmd, promptViewerCmd tea.Cmd
	m.fileList, fileListCmd = m.fileList.Update(msg, m)
	m.promptViewer, promptViewerCmd = m.promptViewer.Update(msg, m)
	return m, tea.Batch(fileListCmd, promptViewerCmd)
}

func (m model) View() string {
	if m.err != nil {
		return fmt.Sprintf("Error: %v", m.err)
	}
	if m.loading {
		return fmt.Sprintf("\n\n   %s Loading files...\n\n", m.spinner.View())
	}
	if m.showPrompt {
		return m.promptViewer.View()
	}
	return m.fileList.View()
}

func (m model) RunProgram() tea.Msg {
	traversal, err := NewRepoTraversal(m.searchPath, m.useGit, m.ignoreTestFiles)
	if err != nil {
		fmt.Printf("Error creating RepoTraversal: %v\n", err)
		os.Exit(1)
	}

	finalFiles, err := traversal.WalkRepo()
	if err != nil {
		fmt.Printf("Error walking repository: %v\n", err)
		os.Exit(1)
	}

	return fileListMsg{finalFiles: finalFiles}
}

func main() {
	var useGit bool
	var ignoreTestFiles bool

	rootCmd := &cobra.Command{
		Use:   "aicp",
		Short: "AI Copy/Paste - A tool for AI copy and paste functionality",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) < 1 {
				fmt.Println("[aicp] - please provide a path (relative or absolute)")
				os.Exit(1)
			}
			ap, _ := getAbsolutePath(args[0])

			m := initialModel(ap, useGit, ignoreTestFiles)

			p := tea.NewProgram(m, tea.WithOutput(os.Stdout)) // Redirect output to stdout
			if _, err := p.Run(); err != nil {
				fmt.Printf("Error running program: %v", err)
				os.Exit(1)
			}
		},
	}

	rootCmd.Flags().BoolVarP(&useGit, "useGit", "g", true, "Ignore files based on .gitignore rules")
	rootCmd.Flags().BoolVarP(&ignoreTestFiles, "includeTestFiles", "t", true, "Include test files")

	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
