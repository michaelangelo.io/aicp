package main

import (
	"os"
	"path/filepath"
)

func getAbsolutePath(path string) (string, error) {
	if path == "." {
		return os.Getwd()
	}

	if !filepath.IsAbs(path) {
		currentDir, err := os.Getwd()
		if err != nil {
			return "", err
		}
		path = filepath.Join(currentDir, path)
	}

	return filepath.Abs(path)
}
