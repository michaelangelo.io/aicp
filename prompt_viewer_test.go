package main

import (
	"strings"
	"testing"

	tea "github.com/charmbracelet/bubbletea"
)

func TestPromptViewer(t *testing.T) {
	testContent := "Test prompt content"
	pv := newPromptViewer(testContent)

	if pv.content != testContent {
		t.Errorf("Expected content to be %q, got %q", testContent, pv.content)
	}
	if pv.ready {
		t.Error("Expected ready to be false initially")
	}

	model := model{showPrompt: true}
	updatedPv, cmd := pv.Update(tea.WindowSizeMsg{Width: 80, Height: 24}, model)

	// It's okay if no command is returned
	if cmd != nil {
		t.Logf("A command was returned: %v", cmd)
	}

	// The viewport might not be ready immediately after the first update
	if updatedPv.ready {
		// Check viewport size only if it's ready
		if updatedPv.viewport.Width != 80 || updatedPv.viewport.Height != 18 { // 24 - 3 for header and footer
			t.Errorf("Unexpected viewport size: %dx%d", updatedPv.viewport.Width, updatedPv.viewport.Height)
		}
	}

	// Test View function
	view := updatedPv.View()
	if len(view) == 0 {
		t.Error("Expected non-empty view")
	}

	// The content might not be visible immediately, so let's check for the header instead
	if !strings.Contains(view, "Prompt Viewer") {
		t.Errorf("View does not contain the expected header")
	}
}
