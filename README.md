<div align="center">
  <img src="https://gitlab.com/michaelangelo.io/aicp/-/raw/main/docs/aicp-logo2.jpg?ref_type=heads" alt="aicp Logo" width="50%">
</div>

# aicp (AI Copy Paste)

aicp is a command-line tool that makes it easy to copy files from your codebase to your clipboard in a format suitable for pasting into chats with Large Language Models (LLMs). It allows you to quickly gather context from your code without tediously copying and pasting individual files.


<div align="center">
  <img src="https://gitlab.com/michaelangelo.io/aicp/-/raw/main/docs/demo.gif?ref_type=heads" alt="aicp Demo GIF" width="70%">
</div>

## Why aicp?

When interacting with LLMs, we often need to provide code snippets or entire files for context. Manually copying and pasting each relevant file can be time-consuming and cumbersome. aicp streamlines this process by allowing you to select the desired files from a directory and automatically copying their contents to the clipboard in a structured format ready for pasting into LLM chats.

## Features

- **Terminal User Interface (TUI)**: aicp provides an intuitive command-line interface for navigating and selecting files.
- **Git Integration**: For security purposes, aicp does not rely on calling the `git` command externally. Instead, it manually parses `.gitignore` files to respect the same ignore rules as git.
- **Search**: find any files in the CLI results.
- **Recursive File Discovery**: aicp is recursive by nature, meaning it will find all relevant files within a given directory and its subdirectories. Currently, it only operates on directories.
- **Repository Detection**: You can open any path inside a git repository, and aicp will automatically detect the repository based on the parent `.git/config` file.
- **File Selection**: Use the spacebar or press `s` to select the files you want to include in the prompt.
- **Prompt Viewing**: Press `p` to view the prompt that has been copied to your clipboard.
- **Relative Path Support**: The input path can be specified relative to the current working directory.

## Demo

Check out this video demonstration of aicp in action:

<div align="center">
  <a href="https://www.youtube.com/watch?v=DEyDyRmkWqc">
    <img src="https://img.youtube.com/vi/DEyDyRmkWqc/0.jpg" alt="aicp Demo Video" width="50%">
  </a>
</div>

## Installation

### Using Homebrew (macOS and Linux)

1. Tap the aicp repository:

```bash
brew tap michaelangelo.io/aicp <https://gitlab.com/michaelangelo.io/aicp>
```

1. Install aicp:

```bash
brew install aicp
```

1. To uninstall:

```bash
brew uninstall aicp && brew untap michaelangelo.io/aicp
```

### Using npm (Node.js)

Install aicp globally using npm:

```bash
npm install -g aicp
```

## Usage

Run aicp followed by the path to the directory you want to copy files from. The path can be relative to the current working directory.

Examples:

```bash
aicp .
aicp ../some-folder
aicp /path/to/folder
```

Once aicp is running:

- Use the arrow keys or `j` and `k` to navigate through the file list.
- Press the spacebar or `s` to select files to include in the prompt.
- Press `p` to view the prompt that has been copied to the clipboard.
- Press `q` or `Esc` to exit aicp.

## Contributing

Contributions are welcome! If you encounter any issues or have suggestions for improvements, please open an issue or submit a merge request on the [aicp GitLab repository](https://gitlab.com/michaelangelo.io/aicp).

## License

aicp is open-source software licensed under the [MIT License](LICENSE).
