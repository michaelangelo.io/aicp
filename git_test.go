package main

import (
	"os"
	"path/filepath"
	"reflect"
	"testing"

	"github.com/go-git/go-billy/v5/memfs"
	"github.com/go-git/go-git/v5/plumbing/format/gitignore"
)

func TestReadPatterns(t *testing.T) {
	fs := memfs.New()

	fs.MkdirAll("test/dir", 0755)
	file, _ := fs.Create("test/.gitignore")
	file.Write([]byte("*.log\n/node_modules\n"))
	file.Close()
	file, _ = fs.Create("test/dir/.gitignore")
	file.Write([]byte("*.tmp\n"))
	file.Close()

	patterns, err := ReadPatterns(fs, []string{"test"})
	if err != nil {
		t.Fatalf("ReadPatterns returned an error: %v", err)
	}

	expectedPatterns := []struct {
		path     []string
		isDir    bool
		expected gitignore.MatchResult
	}{
		{[]string{"test", "file.log"}, false, gitignore.Exclude},
		{[]string{"test", "node_modules"}, true, gitignore.Exclude},
		{[]string{"test", "dir", "file.tmp"}, false, gitignore.Exclude},
		{[]string{"test", "file.txt"}, false, gitignore.NoMatch},
		{[]string{"test", "dir", "file.log"}, false, gitignore.Exclude},
	}

	if len(patterns) != 3 {
		t.Fatalf("Expected 3 patterns, got %d", len(patterns))
	}

	for _, exp := range expectedPatterns {
		var result gitignore.MatchResult
		for _, pattern := range patterns {
			result = pattern.Match(exp.path, exp.isDir)
			if result != gitignore.NoMatch {
				break
			}
		}
		if result != exp.expected {
			t.Errorf("For path %v, expected %v but got %v",
				exp.path, exp.expected, result)
		}
	}
}

func TestReplaceTildeWithHome(t *testing.T) {
	tests := []struct {
		input    string
		expected string
		hasError bool
	}{
		{"~/Documents", filepath.Join(os.Getenv("HOME"), "Documents"), false},
		{"/absolute/path", "/absolute/path", false},
		{"relative/path", "relative/path", false},
	}

	for _, test := range tests {
		result, err := ReplaceTildeWithHome(test.input)
		if test.hasError && err == nil {
			t.Errorf("Expected an error for input %s, but got none", test.input)
		}
		if !test.hasError && err != nil {
			t.Errorf("Unexpected error for input %s: %v", test.input, err)
		}
		if result != test.expected {
			t.Errorf("For input %s, expected %s, but got %s", test.input, test.expected, result)
		}
	}
}

func TestGetGitMatcherForPath(t *testing.T) {
	tempDir, err := os.MkdirTemp("", "git-test")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v", err)
	}
	defer os.RemoveAll(tempDir)

	err = os.MkdirAll(filepath.Join(tempDir, ".git"), 0755)
	if err != nil {
		t.Fatalf("Failed to create .git directory: %v", err)
	}
	_, err = os.Create(filepath.Join(tempDir, ".git", "config"))
	if err != nil {
		t.Fatalf("Failed to create config file: %v", err)
	}

	err = os.WriteFile(filepath.Join(tempDir, ".gitignore"), []byte("*.log\n/node_modules\n"), 0644)
	if err != nil {
		t.Fatalf("Failed to create .gitignore file: %v", err)
	}

	matcher, gitConfigPath, err := getGitMatcherForPath(tempDir)
	if err != nil {
		t.Fatalf("getGitMatcherForPath returned an error: %v", err)
	}

	if matcher == nil {
		t.Error("Expected a non-nil matcher, but got nil")
	}

	expectedConfigPath := tempDir
	if gitConfigPath != expectedConfigPath {
		t.Errorf("Expected git config path %s, but got %s", expectedConfigPath, gitConfigPath)
	}

	if !(*matcher).Match([]string{"test.log"}, false) {
		t.Error("Expected *.log to be ignored, but it wasn't")
	}
	if !(*matcher).Match([]string{"node_modules"}, false) {
		t.Error("Expected /node_modules to be ignored, but it wasn't")
	}
	if (*matcher).Match([]string{"test.txt"}, false) {
		t.Error("Expected test.txt not to be ignored, but it was")
	}
}

func TestFindParentGitConfig(t *testing.T) {
	// Create a temporary directory structure for testing
	tempDir, err := os.MkdirTemp("", "git-config-test")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v", err)
	}
	defer os.RemoveAll(tempDir)

	// Create a mock .git directory and config file
	gitDir := filepath.Join(tempDir, "project", ".git")
	err = os.MkdirAll(gitDir, 0755)
	if err != nil {
		t.Fatalf("Failed to create .git directory: %v", err)
	}
	_, err = os.Create(filepath.Join(gitDir, "config"))
	if err != nil {
		t.Fatalf("Failed to create config file: %v", err)
	}

	// Test cases
	testCases := []struct {
		path     string
		expected string
	}{
		{filepath.Join(tempDir, "project", "src"), filepath.Join(tempDir, "project")},
		{filepath.Join(tempDir, "project"), filepath.Join(tempDir, "project")},
		{tempDir, ""},
	}

	for _, tc := range testCases {
		result := findParentGitConfig(tc.path)
		if !reflect.DeepEqual(result, tc.expected) {
			t.Errorf("For path %s, expected %s, but got %s", tc.path, tc.expected, result)
		}
	}
}
