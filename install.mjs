import os from "os";
import fs from "fs/promises";
import path from "path";
import { execSync } from "child_process";
import { fileURLToPath } from "url";

// Get the current directory
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

// Read package.json to get the version
const packageJsonPath = path.join(__dirname, "package.json");
const packageJson = JSON.parse(await fs.readFile(packageJsonPath, "utf8"));
const version = packageJson.version;

const platform = os.platform();
const arch = os.arch();

let platformName;
switch (platform) {
  case "win32":
    platformName = "windows";
    break;
  case "linux":
    platformName = "linux";
    break;
  case "darwin":
    platformName = "darwin";
    break;
  default:
    console.error(`Unsupported platform: ${platform}`);
    process.exit(1);
}

let archName;
switch (arch) {
  case "x64":
    archName = "amd64";
    break;
  case "arm64":
    archName = "arm64";
    break;
  default:
    console.error(`Unsupported architecture: ${arch}`);
    process.exit(1);
}

const url = `https://gitlab.com/michaelangelo.io/aicp/-/releases/v${version}/downloads/aicp_${version}_${platformName}_${archName}.tar.gz`;

const destDir = path.join(__dirname, "bin");
await fs.mkdir(destDir, { recursive: true });

async function downloadAndExtract(url, destDir) {
  const res = await fetch(url);
  if (!res.ok) throw new Error(`Failed to fetch ${url}: ${res.statusText}`);

  const buffer = Buffer.from(await res.arrayBuffer());
  const destPath = path.join(destDir, path.basename(url));
  await fs.writeFile(destPath, buffer);

  execSync(`tar -xzf ${destPath} -C ${destDir}`);
  await fs.unlink(destPath);

  const binaryName = platform === "win32" ? "aicp.exe" : "aicp";
  await fs.rename(path.join(destDir, binaryName), path.join(destDir, "aicp"));
  await fs.chmod(path.join(destDir, "aicp"), "755");
}

try {
  await downloadAndExtract(url, destDir);
  console.log(`aicp ${version} installed successfully`);
} catch (err) {
  console.error(`Failed to install aicp: ${err.message}`);
  process.exit(1);
}
